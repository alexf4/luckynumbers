__author__ = 'alexf4'

import  sys

def checkPrime(inputInt):
    startpoint = 1.0 * inputInt
    prime = True

    if (inputInt == 1):
        prime = False
    for number in range(2, inputInt):
        if startpoint/number == int(startpoint/number):
            prime = False
    return prime


def F(n):
    if n == 0: return 0
    elif n == 1: return 1
    else: return F(n-1)+F(n-2)

def main():

#Grab the input data
    T =  sys.stdin.readline()

    strings = []
    solutions = []

    #Clean input
    for repeater in range (0,int(T)):

        score = 0

        inputdata = sys.stdin.readline()
        strings.append(inputdata.rstrip())
        textvalue = strings[0]
        valueArray = textvalue.split()
        numarray = []
        numarray.append(int(valueArray[0]))
        numarray.append(int(valueArray[1]))

        start = numarray[0]

        while (start <= numarray[1]):
            if (sumOfDigits(start) and sumOfSquares(start)):
                score = score + 1
            start = start +1

        solutions.append(score)
        strings = []
    for item in solutions:
        print item

def sumOfDigits(input):
    #find the sum of the digits
   suminput =  sum(map(int,str(input)))

    #check if sum is prime
   if checkPrime(suminput):
       return True
   return False

def sumOfSquares(input):
    #find the sum of the squares
    dataArray  =  map(int,str(input))

    suminput = dataArray

    squares = sum(map(lambda a: a*a, suminput))

    #check if sum is prime
    if checkPrime(squares):
        return True
    return False




if __name__ == "__main__":
    main()